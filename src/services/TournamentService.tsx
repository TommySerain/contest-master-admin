import axios from "axios";

class TournamentService {

    static async getAllTournaments() {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_SERVER_URL}/tournament`);
        return response.data;
    } catch (error: any) {
        throw error.response.data;
    }
    
}

export default TournamentService;