import axios from "axios";


interface User {
    id: number;
    name: string;
    pseudo: string;
    description: string;
    email:string;
    image:string;
    score:number;
    isBot:boolean;
    city:any;
    tournament:any;
}

class UserService {
    
    static async getAllUsers() {
        const response = await axios.get('http://localhost:8080/api/user');
        return response.data;
    } catch (error: any) {
        throw error.response.data;
    }
    
}

export default UserService;