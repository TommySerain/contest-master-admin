import LayoutAdmin from '@/src/components/Layouts/admin/LayoutAdmin'
import AdminIndexView from '@/src/views/AdminIndexView'
import React from 'react'

function index() {
    return (
        <LayoutAdmin>
            <AdminIndexView />
        </LayoutAdmin>
    )
}

export default index
