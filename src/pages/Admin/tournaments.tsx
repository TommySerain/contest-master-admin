import LayoutAdmin from '@/src/components/Layouts/admin/LayoutAdmin'
import AdminTournamentView from '@/src/views/AdminTournamentView'
import React from 'react'

function tournaments() {
  return (
    <LayoutAdmin>
            <AdminTournamentView />
    </LayoutAdmin>
  )
}

export default tournaments
