import LayoutAdmin from '@/src/components/Layouts/admin/LayoutAdmin'
import AdminUserView from '@/src/views/AdminUserView'
import React from 'react'

function users() {
    return (
        <LayoutAdmin>
            <AdminUserView />
        </LayoutAdmin>
    )
}

export default users
