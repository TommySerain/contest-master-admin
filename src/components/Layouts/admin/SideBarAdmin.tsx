import Link from 'next/link'
import React, { useState } from 'react'

function SideBarAdmin() {
  return (
    <header
        className={`h-screen lg:w-1/6 bg-cm-black
        lg:sticky z-10 top-0 left-0 text-md text-center
        justify-start hidden text-white lg:flex lg:flex-col items-center
        p-5 gap-10`}
        >
            <nav
            className={`block h-fit `}
            >
                <ul className='flex flex-col gap-8'>
                    <li>
                        <Link href={`#`}>
                            <button className={` font-medium uppercase my-10`}>Retour au site</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`/Admin`}>
                            <button className={` font-medium uppercase mb-10`}>Statistiques</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`/Admin/users`}>
                            <button className={` font-medium uppercase`}>Utilisateurs</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`/Admin/tournaments`}>
                            <button className={` font-medium uppercase`}>Tournois</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`#`}>
                            <button className={` font-medium uppercase`}>Villes</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`#`}>
                            <button className={` font-medium uppercase`}>Parties</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`#`}>
                            <button className={` font-medium uppercase`}>Plateforme</button>
                        </Link>
                    </li>
                    <li>
                        <Link href={`#`}>
                            <button className={` font-medium uppercase`}>Jeux</button>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
  )
}

export default SideBarAdmin
