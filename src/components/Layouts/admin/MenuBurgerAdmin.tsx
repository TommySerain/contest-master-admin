import Image from 'next/image'
import React from 'react'
import Link from 'next/link'
import NavDrawerAdmin from './NavDrawerAdmin'


function MenuBurgerAdmin() {
  return (
    <header className={`absolute bg-cm-gray top-0 z-10 text-white flex justify-end w-screen`}>
        
        <div className='w-[14vw] flex justify-center items-center bg-cm-gray'>
          <NavDrawerAdmin/>
        </div>
    </header>
  )
}

export default MenuBurgerAdmin
