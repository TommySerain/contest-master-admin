import React from "react";
import Head from "next/head";
import HeaderAdmin from "./HeaderAdmin";
import Loading from "../Loading";

function LayoutAdmin({ children }: any) {
    return (
        <div
        className={` bg-cm-black text-white`}
        >
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <meta name="keywords" content={"keywords"} />
                <meta name="description" content={"Création de tournois"} />
                <meta charSet="utf-8" />
                <title>{"CONTEST MASTER - Accueil"}</title>
            </Head>
            <Loading />
            <main className={`lg:flex`}>
                <HeaderAdmin />
                <div className="flex flex-col lg:w-5/6 border-l-2 border-cm-yellow p-10">
                    {children}
                </div>
            </main>
            
        </div>
    );
}

export default LayoutAdmin;
