import { Bars3Icon, XMarkIcon } from '@heroicons/react/16/solid';
import Link from 'next/link';
import React, { useEffect, useState } from 'react'


function NavDrawerAdmin() {
  const [openDrawer, setOpenDrawer] = useState(false);
  const [isSmallScreen, setIsSmallScreen] = useState(false);

  const updateScreenSize = () => {
    setIsSmallScreen(window.innerWidth < 1024);
  };

  useEffect(() => {
    updateScreenSize();
    window.addEventListener('resize', updateScreenSize);

    return () => {
      window.removeEventListener('resize', updateScreenSize);
    };
  }, []);

  return (
    <>
        <button>
            <Bars3Icon
                onClick={() => {
                    setOpenDrawer(true);
                }}
                className="w-full px-3 icon-button lg:hidden"
            />
        </button>
        {openDrawer && isSmallScreen && (
            <div
                onClick={() => {
                    setOpenDrawer(false);
                }}
                className="bg-black/20 z-10 backdrop-blur fixed top-0 left-0 w-full h-full"
            ></div>
        )}
         {isSmallScreen &&(
            <aside
            className={`fixed top-0 right-0 z-30 h-screen w-4/6 bg-cm-black p-6
            flex flex-col gap-3 transform ease-in-out duration-500
            ${openDrawer ? "translate-x-0" : "translate-x-full"}
            `}>
                <div className="flex w-full justify-end">
                    <button
                        onClick={() => {
                            setOpenDrawer(false);
                        }}
                    >
                        <XMarkIcon className="w-12" />
                    </button>
                </div>
                <nav className="basis-auto h-full w-full flex py-2 px-4 text-xl font-medium text-start">
                    <ul className='flex flex-col gap-8'>
                        <li>
                            <Link href={`/`}>
                                <button className={` font-medium uppercase`}>Retour au site</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`/Admin`}>
                                <button className={` font-medium uppercase`}>Statistiques</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`/Admin/users`}>
                                <button className={` font-medium uppercase`}>Utilisateurs</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`/Admin/tournaments`}>
                                <button className={` font-medium uppercase`}>Tournois</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`#`}>
                                <button className={` font-medium uppercase`}>Villes</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`#`}>
                                <button className={` font-medium uppercase`}>Parties</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`#`}>
                                <button className={` font-medium uppercase`}>Plateforme</button>
                            </Link>
                        </li>
                        <li>
                            <Link href={`#`}>
                                <button className={` font-medium uppercase`}>Jeux</button>
                            </Link>
                        </li>
                    </ul>
                </nav>
            </aside>
         )}
        
    </>
  )
}

export default NavDrawerAdmin
