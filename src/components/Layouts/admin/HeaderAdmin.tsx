import React, { useState } from 'react';
import SideBarAdmin from '../admin/SideBarAdmin';
import MenuBurgerAdmin from '../admin/MenuBurgerAdmin';

function HeaderAdmin(props:any) {
    
    return (
        <>
            <SideBarAdmin />
            <MenuBurgerAdmin />
        </>        
    );
}

export default HeaderAdmin;
