import React from 'react'
import LayoutAdmin from '../components/Layouts/admin/LayoutAdmin'

function AdminIndexView() {
    return (
        <section>
            <h1 className='mb-10 text-2xl'>Bienvenue dans l'administration de Contest Master</h1>
        </section>
    )
}

export default AdminIndexView
