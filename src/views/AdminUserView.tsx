import { PencilIcon, PencilSquareIcon, TrashIcon } from '@heroicons/react/16/solid'
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import UserService from '../services/UserService';
import { UserInterface } from '../model/user-interface';




function AdminUserView() {

    const [users, setUsers] = useState<UserInterface[]>([]);

    useEffect(() => {
        const fetchUsers = async () => {
        try {
            let users:UserInterface[] = await UserService.getAllUsers();
            setUsers(users);
            console.log(users);
        } catch (error) {
            console.error(error);
        }
        };

        fetchUsers();
    }, []);

    return (
        <section>
            <h1 className='mb-10 text-2xl'>Utilisateurs de l'application</h1>

            <table className='table-auto w-full'>
                <thead>
                    <tr className='border-cm-yellow'>
                    <th className='border-cm-yellow border-2'>{`Id`}</th>
                    <th className='border-cm-yellow border-2'>{`Nom`}</th>
                    <th className='border-cm-yellow border-2'>{`Pseudo`}</th>
                    <th className='border-cm-yellow border-2'>{`Description`}</th>
                    <th className='border-cm-yellow border-2'>{`Email`}</th>
                    <th className='border-cm-yellow border-2'>{`Image Url`}</th>
                    <th className='border-cm-yellow border-2'>{`Ville`}</th>
                    <th className='border-cm-yellow border-2'>{`Action`}</th>
                </tr>
                </thead>
                <tbody>
                    {users.map((user, index) => (
                        <tr key={index} className='text-center border-cm-yellow border-2'>
                            <td className='border-cm-yellow border-2'>{user.id}</td>
                            <td className='border-cm-yellow border-2'>{user.name}</td>
                            <td className='border-cm-yellow border-2'>{user.pseudo}</td>
                            <td className='border-cm-yellow border-2'>{user.description}</td>
                            <td className='border-cm-yellow border-2'>{user.email}</td>
                            <td className='border-cm-yellow border-2'>{user.image}</td>
                            <td className='border-cm-yellow border-2'>{user.city.id}</td>
                            <td className='flex justify-evenly'>
                                <div className='w-7 text-red-700'>
                                <TrashIcon />
                                </div>
                                <div className='w-7 text-blue-700'>
                                <PencilIcon />
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </section>
    )
}

export default AdminUserView
