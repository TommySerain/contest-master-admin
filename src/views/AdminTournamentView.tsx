import React, { useEffect, useState } from 'react'
import { TournamentInterface } from '../model/tournament-interface';
import TournamentService from '../services/TournamentService';
import { PencilIcon, TrashIcon } from '@heroicons/react/16/solid';

function AdminTournamentView() {
    const [tournaments, setTournaments] = useState<TournamentInterface[]>([]);

    useEffect(() => {
        const fetchTournaments = async () => {
        try {
            let tournaments:TournamentInterface[] = await TournamentService.getAllTournaments();
            setTournaments(tournaments);
            console.log(tournaments);
        } catch (error) {
            console.error(error);
        }
        };

        fetchTournaments();
    }, []);
  return (
    <section>
            <h1 className='mb-10 text-2xl'>Utilisateurs de l'application</h1>

            <table className='table-auto w-full'>
                <thead>
                    <tr className='border-cm-yellow'>
                    <th className='border-cm-yellow border-2'>{`Id`}</th>
                    <th className='border-cm-yellow border-2'>{`Nom`}</th>
                    <th className='border-cm-yellow border-2'>{`Participants`}</th>
                    <th className='border-cm-yellow border-2'>{`Début`}</th>
                    <th className='border-cm-yellow border-2'>{`Image Url`}</th>
                    <th className='border-cm-yellow border-2'>{`Ville`}</th>
                    <th className='border-cm-yellow border-2'>{`Prix`}</th>
                    <th className='border-cm-yellow border-2'>{`Inscription`}</th>
                    <th className='border-cm-yellow border-2'>{`Description`}</th>
                    <th className='border-cm-yellow border-2'>{`Plateforme`}</th>
                    <th className='border-cm-yellow border-2'>{`Jeu`}</th>
                    <th className='border-cm-yellow border-2'>{`Organisateur`}</th>
                    <th className='border-cm-yellow border-2'>{`Action`}</th>
                </tr>
                </thead>
                <tbody>
                    {tournaments.map((tournament:TournamentInterface, index) => (
                        
                        <tr key={index} className='text-center border-cm-yellow border-2'>
                            <td className='border-cm-yellow border-2'>{tournament.id}</td> 
                            <td title={tournament.name} className='border-cm-yellow border-2 cursor-default'>{tournament.name.substring(0,10)+'...'}</td>
                            <td className='border-cm-yellow border-2'>{tournament.participantNbr}</td>
                            <td title={tournament.beginDate} className='cursor-default border-cm-yellow border-2'>{new Date(tournament.beginDate).toLocaleDateString()}</td>
                            <td className='border-cm-yellow border-2'>{tournament.image}</td>
                            <td className='border-cm-yellow border-2'>{tournament.city.id}</td>
                            <td title={tournament.reward} className='cursor-default border-cm-yellow border-2'>{tournament.reward.substring(0,7)+'...'}</td>
                            <td className='border-cm-yellow border-2'>{tournament.entryPrice}</td>
                            <td title={tournament.description} className='cursor-default border-cm-yellow border-2'>{tournament.description.substring(0,7)+'...'}</td>
                            <td className='border-cm-yellow border-2'>{tournament.platform.id}</td>
                            <td className='border-cm-yellow border-2'>{tournament.game.id}</td>
                            <td className='border-cm-yellow border-2'>{tournament.planner.id}</td>
                            <td className='flex justify-evenly items-center'>
                                <div className='w-7 text-red-700'>
                                <TrashIcon />
                                </div>
                                <div className='w-7 text-blue-700'>
                                <PencilIcon />
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </section>
  )
}

export default AdminTournamentView
