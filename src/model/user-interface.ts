export interface UserInterface {
    id: number;
    name: string;
    pseudo: string;
    description: string;
    email:string;
    image:string;
    score:number;
    isBot:boolean;
    city:any;
    tournament:UserInterface;
}