import { UserInterface } from "./user-interface";

export interface TournamentInterface{
    id: number;
    name: string;
    team: boolean;
    participantNbr: number;
    beginDate: string;
    endDate: string;
    image: string;
    address: string;
    reward: string;
    entryPrice: number;
    description: string;
    roundYype: any;
    isPublic:boolean;
    city:any;
    type:any;
    platform:any;
    game:any;
    planner:UserInterface;
}